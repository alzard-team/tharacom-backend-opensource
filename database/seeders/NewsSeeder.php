<?php

namespace Database\Seeders;

use App\Models\Admin\News;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       News::factory(3)->create();
    }
}
