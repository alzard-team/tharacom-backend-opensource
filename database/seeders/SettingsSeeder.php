<?php

namespace Database\Seeders;

use App\Models\Admin\Settings;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::create([

            'site_favicon' => '/uploads/admin/settings/favicon.png',
            'site_logo' => '/uploads/admin/settings/logo.png',
            'features_video_id' => 'UbvlP_5sWOc',
            'features_video_background' => '/uploads/admin/settings/video-bg.jpg',
            'contacts_call' => '+966 11890556',
            'contacts_email' => 'info@tharacom.com',
            'location_on_map_url' => url('/'),
            'google_play_url' => url('/'),
            'app_store_url' => url('/'),
            'url_facebook' => url('/'),
            'url_twitter' => url('/'),
            'url_behance' => '',
            'url_dribbble' => '',
            'url_linkedin' => url('/'),
            'url_instagram' => url('/'),
            'url_youtube' => url('/'),
            'url_whatsapp' => '',
            'url_vimeo' => '',
            'url_rss' => '',
            'dark_mode' => true,
            'navbar_variants' => 'navbar-dark',
            'accent_color_variants' => 'accent-danger',
            'dark_sidebar_variants' => 'sidebar-dark-primary',
            'brand_logo_variants' => 'navbar-dark',

            'en' => [
                'site_title' => 'Tharacom',
                'hero_section_title' => 'Get Discounts & Increase Your Sale',
                'hero_section_description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since',
                'about_title' => 'The Best Way to Give Discounts',
                'about_description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting thereindustry.',
                'about_vision' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'about_mission' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'about_value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'privacy_policy_content' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since',
            ],
            'ar' => [
                'site_title' => 'ثرائكم',
                'hero_section_title' => 'احصل على خصومات وزد مبيعاتك',
                'hero_section_description' => 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة',
                'about_title' => 'أفضل طريقة لتقديم الخصومات',
                'about_description' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد هناك.',
                'about_vision' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'about_mission' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'about_value' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'privacy_policy_content' => 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة',
            ],
        ]);
    }
}
