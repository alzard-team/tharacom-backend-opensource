<?php

namespace Database\Factories\Admin;

use App\Models\Admin\News;
use Illuminate\Database\Eloquent\Factories\Factory;

class NewsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = News::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'en' => [
                'title' => 'First News Title Here Please Lorem Ipsum is Simply',
                'description' => '<p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry There are many
                            variations of passages of Lorem
                            Ipsum available, but the majority have suffered alteration in some form, by injected humour,
                            or randomised words which
                            don’t look even slightly believable. As students across the globe continue to see their
                            learning plans significantly.
                        </p>
                        <blockquote>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry There are many
                            variations of passages of Lorem
                            Ipsum available, but the majority have suffered alteration in some form, by injected humour,
                            or randomised words which
                            don’t look even slightly believable. As students across .
                        </blockquote>
                        <h2 class="subtitle">Sub Title Here</h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry There are many
                            variations of passages of Lorem
                            Ipsum available, but the majority have suffered alteration in some form, by injected humour,
                            or randomised words which
                            don’t look even slightly believable. As students across the globe continue to see their
                            learning plans significantly.
                        </p>',
            ],
            'ar' => [
                'title' => 'عنوان الخبر الأول هنا من فضلك لوريم إيبسوم بكل بساطة',
                'description' => '<p>
                            لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد. هناك الكثير
                            تنويعات مقاطع لوريم
                            Ipsum متاح ، لكن الغالبية عانت من تغيير في شكل ما ، عن طريق الفكاهة المحقونة ،
                            أو الكلمات العشوائية التي
                            لا تبدو معقولة حتى قليلاً. بينما يستمر الطلاب في جميع أنحاء العالم في رؤية ملفات
                            خطط التعلم بشكل كبير.
                        </p>
                        <blockquote>
                            لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد. هناك الكثير
                            تنويعات مقاطع لوريم
                            Ipsum متاح ، لكن الغالبية عانت من تغيير في شكل ما ، عن طريق الفكاهة المحقونة ،
                            أو الكلمات العشوائية التي
                            لا تبدو معقولة حتى قليلاً. بينما يستمر الطلاب في جميع أنحاء العالم في رؤية ملفات
                            خطط التعلم بشكل كبير.
                        </blockquote>
                        <h2 class="subtitle">العنوان الفرعي هنا</h2>
                        <p>
                            لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد. هناك الكثير
                            تنويعات مقاطع لوريم
                            Ipsum متاح ، لكن الغالبية عانت من تغيير في شكل ما ، عن طريق الفكاهة المحقونة ،
                            أو الكلمات العشوائية التي
                            لا تبدو معقولة حتى قليلاً. بينما يستمر الطلاب في جميع أنحاء العالم في رؤية ملفات
                            خطط التعلم بشكل كبير.
                        </p>',
            ],
            'image' => $this->faker->randomElement(['/uploads/admin/news/1.jpg', '/uploads/admin/news/2.jpg', '/uploads/admin/news/3.jpg', '/uploads/admin/news/4.jpg']),
            'slug' => uniqid().rand(1, 100),
            'is_active' => true,
            'order_by' => 0,
            'deleted_at' => null,
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        ];
    }
}
