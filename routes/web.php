<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\IndexController::class, 'index']);
Route::get('/about', [\App\Http\Controllers\IndexController::class, 'about']);
Route::get('/new-partner', [\App\Http\Controllers\IndexController::class, 'new_partner']);
Route::get('/news', [\App\Http\Controllers\IndexController::class, 'news']);
Route::get('/news/{slug}', [\App\Http\Controllers\IndexController::class, 'news_details']);
Route::get('/privacy-policy', [\App\Http\Controllers\IndexController::class, 'privacy_policy']);
Route::get('/contact', [\App\Http\Controllers\IndexController::class, 'contact']);
Route::post('/contact', [\App\Http\Controllers\IndexController::class, 'contact_store']);

Route::get('/search', [\App\Http\Controllers\IndexController::class, 'search']);

Route::get('/new-client', [\App\Http\Controllers\IndexController::class, 'new_client'])->name('new-client');