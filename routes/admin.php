<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\LoginController;


Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});



Route::prefix('admin')->group(function () {

    Route::group(['middleware' => ['auth:admin'], 'as' => 'Admin::'], function() { // For Admin::
        Route::get('/', [App\Http\Controllers\Admin\HomeController::class, 'index']);
       
        Route::resource('categories', App\Http\Controllers\Admin\CategoryController::class);
        Route::resource('news', App\Http\Controllers\Admin\NewsController::class);

    });

});
