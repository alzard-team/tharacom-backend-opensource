<?php

use App\Models\Admin\Admin;

if(!function_exists('iActive'))
{
    function isActive($email) : bool
    {
        $admin = Admin::whereEmail($email)->IsActive()->exists();

        if($admin)
        {
            return true;
        }
        return false;
    }
}
