<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
//    use HasFactory;

    public $table = 'category_translations';

    public $timestamps = false;
    protected $fillable = [
        'title',
        'description',
    ];

}
