<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingsTranslation extends Model
{
    public $table = 'settings_translations';

//    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'site_title',
        'hero_section_title',
        'hero_section_description',
        'about_title',
        'about_description',
        'about_vision',
        'about_mission',
        'about_value',
        'privacy_policy_content',
    ];

}
