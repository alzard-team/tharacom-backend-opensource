<?php

namespace App\Models\Admin;

use Astrotomic\Translatable\Translatable;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Category
 * @package App\Models
 * @version October 28, 2021, 9:44 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $categoryTranslations
 * @property string $icon
 * @property boolean $is_active
 * @property integer $order_by
 */
class Category extends Model
{
    use Translatable;
    use SoftDeletes;

    use HasFactory;

    public $table = 'categories';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $translatedAttributes = [
        'title',
        'description',
    ];
    public $fillable = [
        'icon',
        'slug',
        'is_active',
        'order_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'icon' => 'string',
        'slug' => 'string',
        'is_active' => 'boolean',
        'order_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'icon' => 'required|image',
        'slug' => 'nullable|string|max:255',
        'en.title' => 'required|string|min:4|max:191',
        'ar.title' => 'required|string|min:4|max:191',
        'en.description' => 'nullable|string',
        'ar.description' => 'nullable|string',
        'is_active' => 'required|boolean',
        'order_by' => 'nullable|integer',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];
    public static $rules_update = [
        'icon' => 'required|image',
        'en.title' => 'required|string|min:4|max:191',
        'ar.title' => 'required|string|min:4|max:191',
        'en.description' => 'nullable|string',
        'ar.description' => 'nullable|string',
        'is_active' => 'required|boolean',
        'order_by' => 'nullable|integer',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function categoryTranslations()
    {
        return $this->hasMany(\App\Models\Admin\CategoryTranslation::class, 'category_id');
    }

    public function getCreatedAt() {
        return $this->created_at->toFormattedDateString(); // OR toDateString()
    }

    public function getUpdatedAt() {
        return $this->updated_at->toFormattedDateString(); // OR toDateString()
    }

    public function scopeActive($query) {
        return $query->where('is_active', true);
    }
}
