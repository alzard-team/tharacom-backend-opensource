<?php

namespace App\Models\Admin;

use Astrotomic\Translatable\Translatable;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class News
 * @package App\Models
 * @version September 29, 2021, 12:15 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $newsTranslations
 * @property string $image
 * @property boolean $is_active
 * @property integer $order_by
 */
class News extends Model
{
    use Translatable;
    use SoftDeletes;

    use HasFactory;

    public $table = 'news';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $translatedAttributes = [
        'title',
        'description',
    ];
    public $fillable = [
        'image',
        'slug',
        'is_active',
        'order_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'slug' => 'string',
        'is_active' => 'boolean',
        'order_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'image' => 'required|image',
        'slug' => 'nullable|string|max:255',
        'en.title' => 'required|string|min:4|max:191',
        'ar.title' => 'required|string|min:4|max:191',
        'en.description' => 'required|string|min:50',
        'ar.description' => 'required|string|min:50',
        'is_active' => 'required|boolean',
        'order_by' => 'nullable|integer',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];
    public static $rules_update = [
        'image' => 'nullable|image',
        'en.title' => 'required|string|min:4|max:191',
        'ar.title' => 'required|string|min:4|max:191',
        'en.description' => 'required|string|min:50',
        'ar.description' => 'required|string|min:50',
        'is_active' => 'required|boolean',
        'order_by' => 'nullable|integer',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function newsTranslations()
    {
        return $this->hasMany(\App\Models\Admin\NewsTranslation::class, 'news_id');
    }

    public function scopeActive($query) {
        return $query->where('is_active', true);
    }
}
