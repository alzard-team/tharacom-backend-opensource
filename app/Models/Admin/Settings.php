<?php

namespace App\Models\Admin;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    use Translatable;

    use HasFactory;

    public $table = 'settings';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $translatedAttributes = [
        'site_title',
        'hero_section_title',
        'hero_section_description',
        'about_title',
        'about_description',
        'about_vision',
        'about_mission',
        'about_value',
        'privacy_policy_content',
    ];

    public $fillable = [
        'site_favicon',
        'site_logo',
        'features_video_id',
        'features_video_background',
        'contacts_call',
        'contacts_email',
        'location_on_map_url',
        'google_play_url',
        'app_store_url',
        'url_facebook',
        'url_twitter',
        'url_behance',
        'url_dribbble',
        'url_linkedin',
        'url_instagram',
        'url_youtube',
        'url_whatsapp',
        'url_vimeo',
        'url_rss',
        'dark_mode',
        'navbar_variants',
        'accent_color_variants',
        'dark_sidebar_variants',
        'brand_logo_variants',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'site_favicon' => 'string',
        'site_logo' => 'string',
        'features_video_id' => 'string',
        'features_video_background' => 'string',
        'contacts_call' => 'string',
        'contacts_email' => 'string',
        'location_on_map_url' => 'string',
        'google_play_url' => 'string',
        'app_store_url' => 'string',
        'url_facebook' => 'string',
        'url_twitter' => 'string',
        'url_behance' => 'string',
        'url_dribbble' => 'string',
        'url_linkedin' => 'string',
        'url_instagram' => 'string',
        'url_youtube' => 'string',
        'url_whatsapp' => 'string',
        'url_vimeo' => 'string',
        'url_rss' => 'string',
        'dark_mode' => 'string',
        'navbar_variants' => 'string',
        'accent_color_variants' => 'string',
        'dark_sidebar_variants' => 'string',
        'brand_logo_variants' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

        'en.site_title' => 'required|string|min:4|max:250',
        'ar.site_title' => 'required|string|min:4|max:250',
        'en.hero_section_title' => 'required|string|min:4|max:250',
        'ar.hero_section_title' => 'required|string|min:4|max:250',
        'en.hero_section_description' => 'required|string|min:4|max:5000',
        'ar.hero_section_description' => 'required|string|min:4|max:5000',
        'en.about_title' => 'required|string|min:4|max:250',
        'ar.about_title' => 'required|string|min:4|max:250',
        'en.about_description' => 'required|string|min:4|max:5000',
        'ar.about_description' => 'required|string|min:4|max:5000',
        'en.about_vision' => 'required|string|min:4|max:5000',
        'ar.about_vision' => 'required|string|min:4|max:5000',
        'en.about_mission' => 'required|string|min:4|max:5000',
        'ar.about_mission' => 'required|string|min:4|max:5000',
        'en.about_value' => 'required|string|min:4|max:5000',
        'ar.about_value' => 'required|string|min:4|max:5000',
        'en.privacy_policy_content' => 'required|string|min:100',
        'ar.privacy_policy_content' => 'required|string|min:100',

        'site_favicon' => 'nullable|image|mimes:jpeg,jpg,png',
        'site_logo' => 'nullable|image|mimes:jpeg,jpg,png',
        'features_video_id' => 'required|string|min:4|max:250',
        'features_video_background' => 'nullable|string|min:4|max:250',
        'contacts_call' => 'required|string|min:4|max:250',
        'contacts_email' => 'required|email|min:4|max:250',

        'location_on_map_url' => 'required|url|max:255',
        'google_play_url' => 'nullable|url|max:255',
        'app_store_url' => 'nullable|url|max:255',
        'url_facebook' => 'nullable|url|max:255',
        'url_twitter' => 'nullable|url|max:255',
        'url_behance' => 'nullable|url|max:255',
        'url_dribbble' => 'nullable|url|max:255',
        'url_linkedin' => 'nullable|url|max:255',
        'url_instagram' => 'nullable|url|max:255',
        'url_youtube' => 'nullable|url|max:255',
        'url_whatsapp' => 'nullable|url|max:255',
        'url_vimeo' => 'nullable|url|max:255',
        'url_rss' => 'nullable|url|max:255',

        'dark_mode' => 'nullable|string|min:4|max:250',
        'navbar_variants' => 'nullable|string|min:4|max:250',
        'accent_color_variants' => 'nullable|string|min:4|max:250',
        'dark_sidebar_variants' => 'nullable|string|min:4|max:250',
        'brand_logo_variants' => 'nullable|string|min:4|max:250',


        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function settingsTranslations()
    {
        return $this->hasMany(\App\Models\Admin\SettingsTranslation::class, 'settings_id');
    }
}
