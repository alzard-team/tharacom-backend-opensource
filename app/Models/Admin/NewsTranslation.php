<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsTranslation extends Model
{
    public $table = 'news_translations';

//    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'title',
        'description',
    ];
}
