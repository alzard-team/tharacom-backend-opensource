<?php

namespace App\DataTables;

use App\Helpers\HelpersFun;
use App\Models\Admin\Category;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class CategoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return datatables($query)
            ->addIndexColumn()
            ->addColumn('action', 'admin.categories.datatables_actions')
            ->addColumn('icon', 'admin.categories.icon')
            ->rawColumns(['action', 'icon'])
            ->addColumn('title', function($row){
                return $row->translate('en')->title;
            })
            ->addColumn('is_active', function($row){
                return HelpersFun::isTrue($row->is_active);
            })
            ->addColumn('created_at', function($row){
                return $row->getCreatedAt(); // ->toDayDateTimeString()
            });

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Category $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Category $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'lBfrtip',
                'stateSave' => false,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                ],
                'ordering'=> true,
                'responsive' => true,
                'lengthChange'=> true,
                'autoWidth' => false,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'name' => 'id',
//                'data' => 'id', // DT_RowIndex
                'title' => 'No.',
                'data' => 'DT_RowIndex',
                'orderable' => false,
                'searchable' => false,
            ],[
                'name' => 'title',
                'data' => 'title',
                'title' => trans('admin.Title'),
            ],[
                'name' => 'icon',
                'data' => 'icon',
                'title' => trans('admin.Icon'),
            ],[
                'name' => 'order_by',
                'data' => 'order_by',
                'title' => trans('admin.Order_By'),
            ],[
                'name' => 'created_at',
                'data' => 'created_at',
                'title' => trans('admin.Created_At'),
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'categories_datatable_' . time();
    }
}
