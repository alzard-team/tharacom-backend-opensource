<?php

namespace App\Helpers;

use App\Models\Admin\Category;
use App\Models\Admin\Partner;
use App\Models\Client\Subscribes;
use App\Models\Image;
use App\Models\Receipt;
use App\Models\Admin\Settings;
use App\Models\Admin\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HelpersFun
{

    public static function isTrue($value) {
        if($value) {
            return trans('admin.Yes');
        }
        return trans('admin.No');
    }

    public static function settings()
    {
        if(empty(Session::get('locale'))){
            Session::put('locale', 'en');
        }
        App::setLocale(Session::get('locale'));

        $settings = Settings::first();
        return $settings;
    }

    public static function uploadFile($modal, $file, $attribute, $folder_name)
    {
        if (!empty($file)) {
            $file_name = time() . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/' . $folder_name . '/'), $file_name);

            $file_name = '/uploads/' . $folder_name . '/' . $file_name;
            $modal->{$attribute} = $file_name;
            $modal->save();
        }
    }


    // Number of patients by year and month
    public static function numberOfClientsByYearAndMonth($year, $month) {
        try {
            $clients = Client::whereYear('created_at', $year)->whereMonth('created_at', $month)->count();
        } catch (\Exception $exception) {
            $clients = 0;
        }
        return $clients;
    }

    // get Males And Females Count
    public static function getMalesAndFemalesCount($gender)
    {
        try {
            $gender = Client::where('gender', $gender)->count();
        } catch (\Exception $exception) {
            $gender = 0;
        }
        return $gender;
    }




}
