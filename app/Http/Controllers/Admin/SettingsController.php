<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Settings;
use Illuminate\Http\Request;
use App\Helpers\HelpersFun;
use App\Http\Requests\Admin\UpdateSettingsRequest;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Support\Facades\View;
use Response;


class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        View::composer('*', function ($view) {
            $view->with('titlePage',  $titlePage = trans('admin.Settings'));
            $view->with('activeSettings',  $activeSettings = true);
        });
    }

    /**
     * Display a listing of the Settings.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index()
    {
        $settings = Settings::first();

        return view('admin.settings.index')
            ->with('settings', $settings);
    }

    /**
     * Update the specified Settings in storage.
     *
     * @param int $id
     * @param UpdateSettingsRequest $request
     *
     * @return Response
     */
    public function update(UpdateSettingsRequest $request)
    {
        $settings = Settings::first();

        if (empty($settings)) {
            Flash::error(trans('admin.Settings_not_found'));

            return redirect(route('Admin::settings.index'));
        }

        $settings->update($request->all());
        HelpersFun::uploadFile($settings, $request->site_favicon, 'site_favicon', 'admin/settings');
        HelpersFun::uploadFile($settings, $request->site_logo, 'site_logo', 'admin/settings');

        Flash::success(trans('admin.Settings_updated_successfully'));

        return redirect(route('Admin::settings.index'));
    }

    public function update_colors(Request $request){

        $settings = Settings::first();
        $settings->{$request->item} = $request->color;
        $settings->save();

        return $request->item . ' | ' . $request->color;
    }

}
