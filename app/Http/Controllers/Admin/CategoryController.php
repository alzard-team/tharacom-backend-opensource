<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CategoryDataTable;
use App\Helpers\HelpersFun;
use App\Http\Requests;
use App\Http\Requests\Admin\CreateCategoryRequest;
use App\Http\Requests\Admin\UpdateCategoryRequest;
use App\Repositories\CategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Response;

class CategoryController extends AppBaseController
{
    /** @var  CategoryRepository */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
        $this->middleware('auth:admin');
        View::composer('*', function ($view) {
            $view->with('titlePage',  $titlePage = trans('admin.Categories'));
            $view->with('activeCategories',  $activeCategories = true);
        });
    }

    /**
     * Display a listing of the Category.
     *
     * @param CategoryDataTable $categoryDataTable
     * @return Response
     */
    public function index(CategoryDataTable $categoryDataTable)
    {
        return $categoryDataTable->render('admin.categories.index');
    }

    /**
     * Show the form for creating a new Category.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param CreateCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $request->request->add(['slug' => Str::slug(uniqid())]); //add request
        $input = $request->all();

        $category = $this->categoryRepository->create($input);
        HelpersFun::uploadFile($category, $request->icon, 'icon', 'admin/categories');

        Flash::success(trans('admin.Category_saved_successfully'));

        return redirect(route('Admin::categories.index'));
    }

    /**
     * Display the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error(trans('admin.Category_not_found'));

            return redirect(route('Admin::categories.index'));
        }

        return view('admin.categories.show')->with('category', $category);
    }

    /**
     * Show the form for editing the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error(trans('admin.Category_not_found'));

            return redirect(route('Admin::categories.index'));
        }

        return view('admin.categories.edit')->with('category', $category);
    }

    /**
     * Update the specified Category in storage.
     *
     * @param  int              $id
     * @param UpdateCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryRequest $request)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error(trans('admin.Category_not_found'));

            return redirect(route('Admin::categories.index'));
        }

        $category = $this->categoryRepository->update($request->all(), $id);
        HelpersFun::uploadFile($category, $request->icon, 'icon', 'admin/categories');

        Flash::success(trans('admin.Category_updated_successfully'));

        return redirect(route('Admin::categories.index'));
    }

    /**
     * Remove the specified Category from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error(trans('admin.Category_not_found'));

            return redirect(route('Admin::categories.index'));
        }

        $this->categoryRepository->delete($id);

        Flash::success(trans('admin.Category_deleted_successfully'));

        return redirect(route('Admin::categories.index'));
    }
}
