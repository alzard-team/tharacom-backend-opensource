<?php

return [
    'en' => 'English',
    'ar' => 'العربية',

    'Profile' => 'Profile',
    'Dashboard' => 'Dashboard',
    'Settings' => 'Settings',
    'Admin_Profile' => 'Admin Profile',
    'Avatar' => 'Avatar',
    'Name' => 'Name',
    'Email' => 'Email',
    'Password_Reset' => 'Password Reset',
    'Click_here' => 'Click here',
    'Update' => 'Update',
    'Profile_updated_successfully' => 'Profile updated successfully.',

    'Customize_Dashboard' => 'Customize Dashboard',
    'Navbar_Variants' => 'Navbar Variants',
    'Accent_Color_Variants' => 'Accent Color Variants',
    'Dark_Sidebar_Variants' => 'Dark Sidebar Variants',
    'Brand_Logo_Variants' => 'Brand Logo Variants',
    'Copyright' => 'Copyright',
    'All_rights_reserved' => 'All rights reserved',

    'Edit_Settings' => 'Edit Settings',
    'General' => 'General',
    'Hero_Section' => 'Hero Section',
    'Features_Section' => 'Features Section',
    'About_Section' => 'About Section',
    'Social_Media' => 'Social Media',
    'Cancel' => 'Cancel',
    'Save' => 'Save',
    'site_favicon' => 'site favicon',
    'site_logo' => 'site logo',
    'site_title' => 'site title',
    'English' => 'English',
    'Arabic' => 'Arabic',
    'contacts_call' => 'Contacts Call',
    'contacts_email' => 'Contacts E-mail',
    'location_on_map_url' => 'Location on Map URL',
    'google_play_url' => 'Foogle Play URL',
    'app_store_url' => 'App Store URL',
    'features_video_background' => 'Features Video Background',
    'features_video_id' => 'Features Video ID',
    'hero_section_title' => 'Hero Section Title',
    'hero_section_description' => 'Hero Section Description',
    'about_title' => 'About Title',
    'about_description' => 'About Description',
    'about_vision' => 'About Vision',
    'about_mission' => 'About Mission',
    'about_value' => 'About Value',
    'Privacy_Policy_Section' => 'Privacy Policy Section',
    'privacy_policy_content' => 'Privacy Policy Content',
    'url_facebook' => 'URL Facebook',
    'url_twitter' => 'URL Twitter',
    'url_behance' => 'URL Behance',
    'url_dribbble' => 'URL Dribbble',
    'url_linkedin' => 'URL Linkedin',
    'url_instagram' => 'URL Instagram',
    'url_youtube' => 'URL Youtube',
    'url_whatsapp' => 'URL Whatsapp',
    'url_vimeo' => 'URL Vimeo',
    'url_rss' => 'URL RSS',
    'Settings_updated_successfully' => 'Settings updated successfully.',
    'Settings_not_found' => 'Settings not found',
    'User_Image' => 'User Image',

    'Partner_saved_successfully' => 'Partner saved successfully.',
    'Partner_updated_successfully' => 'Partner updated successfully.',
    'Partner_not_found' => 'Partner not found.',
    'Partner_deleted_successfully' => 'Partner deleted successfully.',
    'Partners' => 'Partners',
    'Add_New' => 'Add New',
    'Partner_Details' => 'Partner Details',
    'Back' => 'Back',
    'Title' => 'Title',
    'Logo' => 'Logo',
    'Create_Partner' => 'Create Partner',
    'partners' => 'partners',
    'Edit_Partner' => 'Edit Partner',
    'Order_By' => 'Order By',
    'Created_At' => 'Created At',
    'Action' => 'Action',
    'Are_you_sure' => 'Are you sure?',
    'Loading_Logo' => 'Loading Logo',
    'Features' => 'Features',
    'Icon' => 'Icon',
    'Description' => 'Description',
    'Icon_Color' => 'Icon Color',
    'Create_Feature' => 'Create Feature',
    'Edit_Feature' => 'Edit Feature',
    'Feature_saved_successfully' => 'Feature saved successfully.',
    'Feature_updated_successfully' => 'Feature updated successfully.',
    'Feature_not_found' => 'Feature not found',
    'Feature_deleted_successfully' => 'Feature deleted successfully.',
    'You_can_choose_the_appropriate_icon_from_the_following_site' => 'You can choose the appropriate icon from the following site',

    'News' => 'News',
    'Create_News' => 'Create News',
    'Edit_News' => 'Edit News',
    'News_Details' => 'News Details',
    'Image' => 'Image',
    'Is_Active' => 'Is Active',
    'Slug' => 'Slug',
    'Loading_Image' => 'Loading Image',
    'Yes' => 'Yes',
    'No' => 'No',

    'Home' => 'Home',

    'Offers' => 'Offers',
    'Offers_saved_successfully' => 'Offers saved successfully.',
    'Offers_not_found' => 'Offers not found',
    'Offers_updated_successfully' => 'Offers updated successfully.',
    'Offers_deleted_successfully' => 'Offers deleted successfully.',
    'Create_Offers' => 'Create Offers',
    'Edit_Offers' => 'Edit Offers',
    'Discounts' => 'Discounts',
    'Offers_Details' => 'Offers Details',

    'Contacts' => 'Contacts',
    'Contact_saved_successfully' => 'Contact saved successfully.',
    'Contact_updated_successfully' => 'Contact updated successfully.',
    'Contact_not_found' => 'Contact not found',
    'Contact_deleted_successfully' => 'Contact deleted successfully.',
    'Create_Contact' => 'Create Contact',
    'Edit_Contact' => 'Edit Contact',
    'Subject' => 'Subject',
    'Message' => 'Message',
    'Is_Read' => 'Is Read',
    'Contact_Details' => 'Contact Details',

    'Email_Verified_At' => 'Email Verified At',
    'Password' => 'Password',
    'Admins' => 'Admins',
    'Admin_saved_successfully' => 'Admin saved successfully.',
    'Admin_updated_successfully' => 'Admin updated successfully.',
    'Admin_not_found' => 'Admin not found',
    'Admin_deleted_successfully' => 'Admin deleted successfully.',
    'Create_Admin' => 'Create Admin',
    'Edit_Admin' => 'Edit Admin',

    'Partner_Features' => 'Partner Features',
    'PartnerFeatures' => 'Partner Features',
    'Create_Partner_Features' => 'Create Partner Features',
    'Edit_Partner_Features' => 'Edit Partner Features',
    'Partner_Features_Details' => 'Partner Features Details',
    'Partner_Features_saved_successfully' => 'Partner Features saved successfully.',
    'Partner_Features_updated_successfully' => 'Partner Features updated successfully.',
    'Partner_Features_not_found' => 'Partner Features not found',
    'Partner_Features_deleted_successfully' => 'Partner Features deleted successfully.',

    'Packages' => 'Packages',
    'Create_Package' => 'Create Package',
    'Package_Details' => 'Package Details',
    'Edit_Package' => 'Edit Package',
    'Weekly' => 'Weekly',
    'Monthly' => 'Monthly',
    'Yearly' => 'Yearly',
    'weekly' => 'Weekly',
    'monthly' => 'Monthly',
    'yearly' => 'Yearly',
    'Admin' => 'Admin',
    'Uniqid' => 'UniqID',
    'Price' => 'Price',
    'Currency' => 'Currency',
    'Duration' => 'Duration',
    'Package_saved_successfully' => 'Package saved successfully.',
    'Package_updated_successfully' => 'Package updated successfully.',
    'Package_not_found' => 'Package not found',
    'Package_deleted_successfully' => 'Package deleted successfully.',
    'In_order_to_add_a_new_feature_put_the_code_At_the_end_of_each_feature' => 'In order to add a new feature, put the code ( , ) At the end of each feature', // من أجل اضافة ميزة جديدة قم بوضع الرمز ( , ) في نهاية كل ميزة

    'Clients' => 'Clients',
    'Users' => 'Users',
    'Username' => 'Username',
    'Create_Client' => 'Create Client',
    'Edit_Client' => 'Edit Client',
    'Full_Name' => 'Full Name',
    'First_Name' => 'First Name',
    'Last_Name' => 'Last Name',
    'Mobile' => 'Mobile',
    'Nationality' => 'Nationality',
    'State' => 'State',
    'Address' => 'Address',
    'Gender' => 'Gender',
    'Remember_Token' => 'Remember Token',
    'Client_Details' => 'Client Details',
    'Client_saved_successfully' => 'Client saved successfully.',
    'Client_updated_successfully' => 'Client updated successfully.',
    'Client_deleted_successfully' => 'Client deleted successfully.',
    'Client_not_found' => 'Client not found',
    'Male' => 'Male',
    'Female' => 'Female',
    'male' => 'Male',
    'female' => 'Female',

    'Categories' => 'Categories',
    'Create_Category' => 'Create Category',
    'Edit_Category' => 'Edit Category',
    'Category_Details' => 'Category Details',
    'Category_saved_successfully' => 'Category saved successfully',
    'Category_not_found' => 'Category not found',
    'Category_updated_successfully' => 'Category updated successfully',
    'Category_deleted_successfully' => 'Category deleted successfully',

    'Multiple_Select_Category' => 'Multiple Select Category',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
];

