<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'sign_out' => 'Sign Out',
    'email' => 'Email',
    'remember_me' => 'Remember Me',
    'sign_in' => 'Sign In',
    'full_name' => 'Full Name',
    'confirm_password' => 'Confirm Password',
    'register' => 'Register',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    'login' => [
        'title' => 'Login',
        'forgot_password' => 'Forgot Password',
        'register_membership' => 'Register Membership',
    ],
    'forgot_password' => [
      'title' => 'Forgot Password',
      'send_pwd_reset' => 'Send Password Reset',
    ],
    'registration' => [
        'have_membership' => 'Have Membership',
        'terms' => 'Terms',
        'i_agree' => 'is Agree',
    ],
    'reset_password' => [
        'reset_pwd_btn' => 'Reset Password',
    ],
    'app' => [
        'member_since' => 'Member Since',
    ]

];
