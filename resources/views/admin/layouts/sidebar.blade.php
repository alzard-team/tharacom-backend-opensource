<aside id="dark_sidebar_variants" class="main-sidebar {!! HelpersFun::settings()->dark_sidebar_variants !!} elevation-4">
    <a href="{{ route('Admin::home.index') }}" id="brand_logo_variants" class="brand-link {!! HelpersFun::settings()->brand_logo_variants !!}">
        <img src="{!! \App\Helpers\HelpersFun::settings()->site_favicon !!}"
             alt="{{ config('app.name') }} Logo"
             class="brand-image img-circle elevation-3">
        <span class="brand-text font-weight-light">{{ config('app.name') }}</span>
    </a>
    <div class="sidebar">
        <!--  Sidebar user panel (optional)  -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ Auth::user()->avatar }}" class="img-circle elevation-2" alt="@lang('admin.User_Image')">
            </div>
            <div class="info">
                <a href="{!! route('Admin::profile.index') !!}" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @include('admin.layouts.menu')
            </ul>
        </nav>
    </div>
</aside>
