<li class="nav-item">
    <a href="{!! route('Admin::home.index') !!}" class="nav-link {!! isset($activeDashboard) ? 'active' : '' !!}">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>@lang('admin.Dashboard')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('Admin::admins.index') }}"
       class="nav-link {!! isset($activeAdmins) ? 'active' : '' !!}">
        <i class="nav-icon fas fa-users"></i>
        <p>@lang('admin.Admins')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('Admin::clients.index') }}"
       class="nav-link {!! isset($activeClients) ? 'active' : '' !!}">
        <i class="nav-icon fas fa-users"></i>
        <p>@lang('admin.Clients') / @lang('admin.Users')</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('Admin::settings.index') }}"
       class="nav-link {!! isset($activeSettings) ? 'active' : '' !!}">
        <i class="nav-icon fas fa-cogs"></i>
        <p>@lang('admin.Settings')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('Admin::partners.index') }}"
       class="nav-link {!! isset($activePartners) ? 'active' : '' !!}">
        <i class="nav-icon fas fa-handshake"></i>
        <p>@lang('admin.Partners')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('Admin::features.index') }}"
       class="nav-link {!! isset($activeFeatures) ? 'active' : '' !!}">
        <i class="nav-icon fas fa-list"></i>
        <p>@lang('admin.Features')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('Admin::partnerFeatures.index') }}"
       class="nav-link {!! isset($activePartnerFeatures) ? 'active' : '' !!}">
        <i class="nav-icon fas fa-list"></i>
        <p>@lang('admin.PartnerFeatures')</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('Admin::news.index') }}"
       class="nav-link {!! isset($activeNews) ? 'active' : '' !!}">
        <i class="nav-icon fas fa-newspaper"></i>
        <p>@lang('admin.News')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('Admin::categories.index') }}"
       class="nav-link {!! isset($activeCategories) ? 'active' : '' !!}">
        <i class="nav-icon fa fa-list-ol"></i>
        <p>@lang('admin.Categories')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('Admin::packages.index') }}"
       class="nav-link  {!! isset($activePackages) ? 'active' : '' !!}">
        <i class="nav-icon fab fa-wizards-of-the-coast"></i>
        <p>@lang('admin.Packages')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('Admin::offers.index') }}"
       class="nav-link {!! isset($activeOffers) ? 'active' : '' !!}">
        <i class="nav-icon fab fa-buffer"></i>
        <p>@lang('admin.Offers')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('Admin::contacts.index') }}" class="nav-link {!! isset($activeContacts) ? 'active' : '' !!}">
        <i class="nav-icon fas fa-comments"></i>
        <p>@lang('admin.Contacts')</p>
    </a>
</li>
