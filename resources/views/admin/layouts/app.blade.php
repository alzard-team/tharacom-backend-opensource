<!DOCTYPE html>
<html lang="{!! app()->getLocale() !!}">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <link rel="icon" type="image/png" href="{!! HelpersFun::settings()->site_favicon !!}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
          integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
          crossorigin="anonymous"/>

    <!-- Font Glyphicons Halflings -->
    <link rel="stylesheet" href="{!! asset('/adminlte/fonts/glyphicons-halflings.css') !!}">

    <!-- Themify Icons -->
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css'>

    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
          rel="stylesheet">

    <!-- AdminLTE -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.0.5/css/adminlte.min.css"
          integrity="sha512-rVZC4rf0Piwtw/LsgwXxKXzWq3L0P6atiQKBNuXYRbg2FoRbSTIY0k2DxuJcs7dk4e/ShtMzglHKBOJxW8EQyQ=="
          crossorigin="anonymous"/>

    <!-- iCheck -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/icheck-bootstrap/3.0.1/icheck-bootstrap.min.css"
          integrity="sha512-8vq2g5nHE062j3xor4XxPeZiPjmRDh6wlufQlfC6pdQ/9urJkU07NM0tEREeymP++NczacJ/Q59ul+/K2eYvcg=="
          crossorigin="anonymous"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
          integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
          crossorigin="anonymous"/>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          integrity="sha512-aEe/ZxePawj0+G2R+AaIxgrQuKT68I28qh+wgLrcAJOz3rxCP+TwrK5SPN+E5I+1IQjNtcfvb96HDagwrKRdBw=="
          crossorigin="anonymous"/>

    <!-- Select2 -->
    <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">


    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{!! asset('/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') !!}">
    <!-- Theme style For Dark Mode -->
    <link rel="stylesheet" href="{!! asset('/adminlte/dark-mode/css/adminlte.min.css') !!}">

    <link rel="stylesheet" href="{!! asset('/adminlte/dashboard-edit-style.css') !!}">

    @if(app()->getLocale() == 'ar')
        <!-- Bootstrap 4 RTL -->
        <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css">
        <!-- Custom style for RTL -->
        <link rel="stylesheet" href="{!! asset('/adminlte/dist/css/custom.css') !!}">
    @endif


    {{--<!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Glyphicons Halflings -->
    <link rel="stylesheet" href="{!! asset('/adminlte/fonts/glyphicons-halflings.css') !!}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{!! asset('/adminlte/plugins/fontawesome-free/css/all.min.css') !!}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{!! asset('/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') !!}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{!! asset('/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') !!}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{!! asset('/adminlte/plugins/jqvmap/jqvmap.min.css') !!}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{!! asset('/adminlte/dist/css/adminlte.min.css') !!}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{!! asset('/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') !!}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{!! asset('/adminlte/plugins/daterangepicker/daterangepicker.css') !!}">
    <!-- summernote -->
    <link rel="stylesheet" href="{!! asset('/adminlte/plugins/summernote/summernote-bs4.css') !!}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Bootstrap 4 RTL -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css">
    <!-- Custom style for RTL -->
    <link rel="stylesheet" href="{!! asset('/adminlte/dist/css/custom.css') !!}">--}}

    <style>
        .min-width-100{
            min-width: 100px;
        }
        i.ti{font-size: 30px;}
    </style>

    @yield('third_party_stylesheets')

    @stack('page_css')
</head>

<body class="hold-transition {!! HelpersFun::settings()->dark_mode ? 'dark-mode' : '' !!}  sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    <!-- Main Header -->
    <nav id="navbar_variants" class="main-header navbar navbar-expand  {!! HelpersFun::settings()->navbar_variants !!}">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false" >
                    <img src="/{!! app()->getLocale() !!}.png">
                    @lang('admin.'.app()->getLocale())
                    <i class="fa fa-angle-down"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right min-width-100" style="left: inherit; right: 0px;">
                    <div class="dropdown-divider"></div>
                    <a href="{!! url('/locale/en') !!}" class="dropdown-item">
                        <img src="/en.png" alt="" />
                        English
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{!! url('/locale/ar') !!}" class="dropdown-item">
                        <img src="/ar.png" alt="" />
                        العربية
                    </a>
                </div>
            </li>

            <li class="nav-item fullscreen">
                <a class="nav-link" data-widget="fullscreen" href="javascript:;" role="button" data-toggle="tooltip" title="Fullscreen" data-placement="bottom">
                    <i class="fas fa-expand-arrows-alt"></i>
                </a>
            </li>

            <li class="nav-item dropdown user-menu">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ Auth::user()->avatar }}"
                         class="user-image img-circle elevation-2" alt="User Image">
                    <span class="d-none d-md-inline">{{ Auth::user()->name }}</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <!-- User image -->
                    <li class="user-header bg-primary">
                        <img src="{{ Auth::user()->avatar }}"
                             class="img-circle elevation-2"
                             alt="User Image">
                        <p>
                            {{ Auth::user()->name }}
                            <small>@lang('auth.app.member_since') {{ Auth::user()->created_at->format('M. Y') }}</small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <a href="{{ route('Admin::profile.index')  }}" class="btn btn-default btn-flat">@lang('admin.Profile')</a>
                        <a href="#" class="btn btn-default btn-flat float-right"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            @lang('auth.sign_out')
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
                        class="fas fa-th-large"></i></a>
            </li>
        </ul>
    </nav>

    <!-- Left side column. contains the logo and sidebar -->
@include('admin.layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content">
            @yield('content')
        </section>
    </div>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->

        <div
            class="p-3 control-sidebar-content os-host os-theme-dark os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-overflow os-host-overflow-y os-host-transition"
            style="height: 214px;">

            <div class="os-resize-observer-host"><div class="os-resize-observer observed" style="left: 0px; right: auto;"></div></div>

            <div class="os-size-auto-observer" style="height: calc(100% + 1px); float: left;">
                <div class="os-resize-observer observed"></div>
            </div>
            <div class="os-content-glue" style="margin: -16px;"></div>
            <div class="os-padding">
                <div class="os-viewport os-viewport-native-scrollbars-invisible"
                     style="overflow-y: scroll; right: 0px; bottom: 0px;">
                    <div class="os-content" style="padding: 16px; height: 100%; width: 100%;"><h5>@lang('admin.Customize_Dashboard')</h5>
                        <hr class="mb-2">
                        <div class="mb-4">
                            <label>
                                <input onclick="myFunctionForSettingsColor('dark_mode', this)" id="dark_mode" type="checkbox" {!! HelpersFun::settings()->dark_mode ? 'checked' : '' !!} value="{!! HelpersFun::settings()->dark_mode ? '0' : '1' !!}" class="mr-1"><span>Dark Mode</span>
                            </label>
                        </div>

                        <h6>@lang('admin.Navbar_Variants')</h6>
                        <div class="d-flex">

                            <div class="d-flex flex-wrap mb-3">
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-primary')" class="bg-primary elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-secondary')" class="bg-secondary elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-info')" class="bg-info elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-success')" class="bg-success elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-danger')" class="bg-danger elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-indigo')" class="bg-indigo elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-purple')" class="bg-purple elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-pink')" class="bg-pink elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-teal')" class="bg-teal elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-cyan')" class="bg-cyan elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-dark')" class="bg-dark elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-gray-dark')" class="bg-gray-dark elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-dark navbar-gray')" class="bg-gray elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-light')" class="bg-light elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-light navbar-warning')" class="bg-warning elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-light navbar-white')" class="bg-white elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                <div onclick="myFunctionForSettingsColor('navbar_variants', 'navbar-light navbar-orange')" class="bg-orange elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            </div>
                        </div>

                        <h6>@lang('admin.Accent_Color_Variants')</h6>
                        <div class="d-flex"></div>
                        <div class="d-flex flex-wrap mb-3">
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-primary')" class="bg-primary elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-warning')" class="bg-warning elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-info')" class="bg-info elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-danger')" class="bg-danger elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-success')" class="bg-success elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-indigo')" class="bg-indigo elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-navy')" class="bg-navy elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-purple')" class="bg-purple elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-fuchsia')" class="bg-fuchsia elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-pink')" class="bg-pink elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-maroon')" class="bg-maroon elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-orange')" class="bg-orange elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-lime')" class="bg-lime elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-teal')" class="bg-teal elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('accent_color_variants', 'accent-olive')" class="bg-olive elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                        </div>

                        <h6>@lang('admin.Dark_Sidebar_Variants')</h6>
                        <div class="d-flex"></div>
                        <div class="d-flex flex-wrap mb-3">
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-primary')" class="bg-primary elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-warning')" class="bg-warning elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-info')" class="bg-info elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-danger')" class="bg-danger elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-success')" class="bg-success elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-indigo')" class="bg-indigo elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-navy')" class="bg-navy elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-purple')" class="bg-purple elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-fuchsia')" class="bg-fuchsia elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-pink')" class="bg-pink elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-maroon')" class="bg-maroon elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-orange')" class="bg-orange elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-lime')" class="bg-lime elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-teal')" class="bg-teal elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('dark_sidebar_variants', 'sidebar-dark-olive')" class="bg-olive elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                        </div>

                        {{--<h6>Light Sidebar Variants</h6>
                        <div class="d-flex"></div>
                        <div class="d-flex flex-wrap mb-3">
                            <div class="bg-primary elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-warning elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-info elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-danger elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-success elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-indigo elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-navy elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-purple elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-fuchsia elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-pink elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-maroon elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-orange elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-lime elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-teal elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div class="bg-olive elevation-2"
                                 style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                        </div>--}}

                        <h6>@lang('admin.Brand_Logo_Variants')</h6>
                        <div class="d-flex"></div>
                        <div class="d-flex flex-wrap mb-3">
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-primary')" class="bg-primary elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-secondary')" class="bg-secondary elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-info')" class="bg-info elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-success')" class="bg-success elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-danger')" class="bg-danger elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-indigo')" class="bg-indigo elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-purple')" class="bg-purple elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-pink')" class="bg-pink elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-teal')" class="bg-teal elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-cyan')" class="bg-cyan elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-dark')" class="bg-dark elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-gray-dark')" class="bg-gray-dark elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-gray')" class="bg-gray elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-light')" class="bg-light elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-warning')" class="bg-warning elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-white')" class="bg-white elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                            <div onclick="myFunctionForSettingsColor('brand_logo_variants', 'navbar-orange')" class="bg-orange elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable">
                <div class="os-scrollbar-track">
                    <div class="os-scrollbar-handle" style="transform: translate(0px, 0px); width: 100%;"></div>
                </div>
            </div>
            <div class="os-scrollbar os-scrollbar-vertical">
                <div class="os-scrollbar-track">
                    <div class="os-scrollbar-handle" style="transform: translate(0px, 0px); height: 16.7712%;"></div>
                </div>
            </div>
            <div class="os-scrollbar-corner"></div>
        </div>

    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        {{--<div class="float-right d-none d-sm-block">
            <b>Version</b> 3.0.5
        </div>--}}
        <strong  id="accent_color_variants" class="{!! HelpersFun::settings()->accent_color_variants !!}">@lang('admin.Copyright') &copy; {!! date('Y') !!} <a href="{!! url('/') !!}">{!! env('APP_NAME') !!}</a>.</strong> @lang('admin.All_rights_reserved').
    </footer>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
        integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
        crossorigin="anonymous"></script>

@if(app()->getLocale() == 'ar')
    <!-- Bootstrap 4 rtl -->
    <script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js"></script>
@endif


<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>

<!-- AdminLTE App -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.0.5/js/adminlte.min.js"
        integrity="sha512-++c7zGcm18AhH83pOIETVReg0dr1Yn8XTRw+0bWSIWAVCAwz1s2PwnSj4z/OOyKlwSXc4RLg3nnjR22q0dhEyA=="
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"
        integrity="sha512-rmZcZsyhe0/MAjquhTgiUcb4d9knaFc7b5xAfju483gbEXTkeJRUMIPk6s3ySZMYUHEcjKbjLjyddGWMrNEvZg=="
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"
        integrity="sha512-GDey37RZAxFkpFeJorEUwNoIbkTwsyC736KNSYucu1WJWFK9qTdzYub8ATxktr6Dwke7nbFaioypzbDOQykoRg=="
        crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"
        integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A=="
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.min.js"
        integrity="sha512-J+763o/bd3r9iW+gFEqTaeyi+uAphmzkE/zU8FxY6iAvD3nQKXa+ZAWkBI9QS9QkYEKddQoiy0I5GDxKf/ORBA=="
        crossorigin="anonymous"></script>

<script>
    $(function () {
        bsCustomFileInput.init();
    });

    $("input[data-bootstrap-switch]").each(function () {
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
</script>


<!-- Select2 -->
<script src="https://adminlte.io/themes/v3/plugins/select2/js/select2.full.min.js"></script>

{{--<!-- jQuery -->
<script src="{!! asset('/adminlte/plugins/jquery/jquery.min.js') !!}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{!! asset('/adminlte/plugins/jquery-ui/jquery-ui.min.js') !!}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 rtl -->
<script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{!! asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>
<!-- ChartJS -->
<script src="{!! asset('/adminlte/plugins/chart.js/Chart.min.js') !!}"></script>
<!-- Sparkline -->
<script src="{!! asset('/adminlte/plugins/sparklines/sparkline.js') !!}"></script>
<!-- JQVMap -->
<script src="{!! asset('/adminlte/plugins/jqvmap/jquery.vmap.min.js') !!}"></script>
<script src="{!! asset('/adminlte/plugins/jqvmap/maps/jquery.vmap.world.js') !!}"></script>
<!-- jQuery Knob Chart -->
<script src="{!! asset('/adminlte/plugins/jquery-knob/jquery.knob.min.js') !!}"></script>
<!-- daterangepicker -->
<script src="{!! asset('/adminlte/plugins/moment/moment.min.js') !!}"></script>
<script src="{!! asset('/adminlte/plugins/daterangepicker/daterangepicker.js') !!}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{!! asset('/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') !!}"></script>
<!-- Summernote -->
<script src="{!! asset('/adminlte/plugins/summernote/summernote-bs4.min.js') !!}"></script>
<!-- overlayScrollbars -->
<script src="{!! asset('/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') !!}"></script>
<!-- AdminLTE App -->
<script src="{!! asset('/adminlte/dist/js/adminlte.js') !!}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{!! asset('/adminlte/dist/js/pages/dashboard.js') !!}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{!! asset('/adminlte/dist/js/demo.js') !!}"></script>--}}


<!-- overlayScrollbars -->
<script src="{!! asset('/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') !!}"></script>
<!-- ChartJS -->
<script src="{!! asset('/adminlte/plugins/chart.js/Chart.min.js') !!}"></script>
<script src="https://adminlte.io/themes/v3/plugins/chart.js/Chart.min.js"></script>

@stack('third_party_scripts')

@stack('page_scripts')
<script>
    //fullscreen function
    $(".fullscreen").click(function(){
        x = $(this).find("a>i").attr("class") ;
        if(x == "fas fa-expand-arrows-alt")
        {
            showFullscreen(document.documentElement);
            $(this).find("a").html('<i class="fas fa-compress-arrows-alt" data-toggle="tooltip" title="Exit Fullscreen" data-placement="bottom"></i>');
        }else{
            exitFullscreen();
            $(this).find("a").html('<i class="fas fa-expand-arrows-alt" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"></i>');
        }
    });
    //Fullscreen Function
    function showFullscreen(element) {
        if(element.showFullscreen) {
            element.showFullscreen();
        } else if(element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if(element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if(element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    }
    //Exit Fullscreen Function
    function exitFullscreen() {
        if(document.exitFullscreen) {
            document.exitFullscreen();
        } else if(document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if(document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }

</script>

<script>
    function myFunctionForSettingsColor(item, color) {

        // dark_mode
        // navbar_variants
        // accent_color_variants
        // dark_sidebar_variants
        // brand_logo_variants
        console.log(item, color);

        if(item == 'dark_mode') {
            $("body").toggleClass('dark-mode');
        }

        if(item == 'navbar_variants') {
            $("#navbar_variants").removeClass();
            $("#navbar_variants").addClass('main-header navbar navbar-expand ' + color);
        }

        if(item == 'accent_color_variants') {
            $("#accent_color_variants").removeClass();
            $("#accent_color_variants").addClass(color);
        }

        if(item == 'dark_sidebar_variants') {
            $("#dark_sidebar_variants").removeClass();
            $("#dark_sidebar_variants").addClass('main-sidebar ' + color);
        }

        if(item == 'brand_logo_variants') {
            $("#brand_logo_variants").removeClass();
            $("#brand_logo_variants").addClass('brand-link ' + color);
        }

        if(item == 'dark_mode'){
            color = $( color ).val();
            if(color == 1){
                $('input#dark_mode').val(0);
            }else{
                $('input#dark_mode').val(1);
            }
        }

        $.ajax({
            url : "{!! route('Admin::settings.update-colors') !!}",
            type: "get",
            data : {
                item: item,
                color: color
            },

            success: function(data, textStatus, jqXHR)
            {
                //data - response from server
                // console.log('data:' + data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                // console.log('jqXHR: ' + jqXHR)
                location.reload();
            }
        });
    }
</script>
</body>
</html>
