<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('image', trans('admin.Image').':') !!}
    <input type="file" class="form-control @error('image') is-invalid @enderror" name="image" accept="image/*">
</div>

<!-- Title Field EN -->
<div class="form-group col-sm-6">
    {!! Form::label('title', trans('admin.Title').': ('.trans('admin.English') .')') !!}
    {!! Form::text('en[title]', null, ['class' => 'form-control']) !!}
</div>
<!-- Title Field AR -->
<div class="form-group col-sm-6">
    {!! Form::label('title', trans('admin.Title').': ('.trans('admin.Arabic') .')') !!}
    {!! Form::text('ar[title]', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field EN -->
<div class="form-group col-sm-12">
    {!! Form::label('description', trans('admin.Description').': ('.trans('admin.English') .')') !!}
    <textarea class="form-control summernote" name="en[description]"></textarea>
</div>
<!-- Description Field AR -->
<div class="form-group col-sm-12">
    {!! Form::label('description', trans('admin.Description').': ('.trans('admin.Arabic') .')') !!}
    <textarea class="form-control summernote" name="ar[description]"></textarea>
</div>



<!-- Is Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_active', trans('admin.Is_Active').':') !!}
    {!! Form::select('is_active', [1 => trans('admin.Yes'), 0 => trans('admin.No')], null, ['class' => 'form-control']) !!}
</div>


<!-- Order By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_by', trans('admin.Order_By').':') !!}
    {!! Form::number('order_by', null, ['class' => 'form-control']) !!}
</div>
