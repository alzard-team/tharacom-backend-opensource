@extends('admin.layouts.app')
@section('title', HelpersFun::settings()->site_title . ' | ' . $titlePage)
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>@lang('admin.Edit_News')</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($news, ['route' => ['Admin::news.update', $news->id], 'method' => 'patch', 'enctype'=>'multipart/form-data']) !!}

            <div class="card-body">
                <div class="row">
                    @include('admin.news.fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit(trans('admin.Save'), ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('Admin::news.index') }}" class="btn btn-default">@lang('admin.Cancel')</a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection


@push('page_css')
    <!-- summernote -->
    <link href="//cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    {{--<link rel="stylesheet" href="{!! asset('/adminlte/plugins/summernote/css/summernote-bs4.min.css') !!}">--}}
@endpush

@push('page_scripts')
    <!-- Summernote -->
    <script src="//cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    {{--<script src="{!! asset('/adminlte/plugins/summernote/js/summernote-bs4.min.js') !!}"></script>--}}
    <script>
        $(function () {
            // Summernote
            $('.summernote').summernote();

            // CodeMirror
            /*CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });*/
        })
    </script>
@endpush
