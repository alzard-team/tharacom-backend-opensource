<!-- Image Field -->
<div class="col-sm-6">
    {!! Form::label('image', trans('admin.Image').':') !!}
    <p><img src="{!! url($news->image) !!}" width="200px" alt="@lang('admin.Loading_Image')"></p>
</div>

<!-- Slug Field -->
<div class="col-sm-6">
    {!! Form::label('slug', trans('admin.Slug').':') !!}
    <p>{{ $news->slug }}</p>
</div>

<!-- Title Field -->
<div class="col-sm-6">
    {!! Form::label('title', trans('admin.Title').': ('.trans('admin.English') .')') !!}
    <p>{!! $news->translate('en')->title !!}</p>
</div>
<!-- Title Field -->
<div class="col-sm-6">
    {!! Form::label('title', trans('admin.Title').': ('.trans('admin.Arabic') .')') !!}
    <p>{!! $news->translate('ar')->title !!}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', trans('admin.Description').': ('.trans('admin.English') .')') !!}
    <p>{!! $news->translate('en')->description !!}</p>
</div>
<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', trans('admin.Description').': ('.trans('admin.Arabic') .')') !!}
    <p>{!! $news->translate('ar')->description !!}</p>
</div>


<!-- Is Active Field -->
<div class="col-sm-6">
    {!! Form::label('is_active', trans('admin.Is_Active').':') !!}
    <p>{{ HelpersFun::isTrue($news->is_active) }}</p>
</div>

<!-- Order By Field -->
<div class="col-sm-12">
    {!! Form::label('order_by', trans('admin.Order_By').':') !!}
    <p>{{ $news->order_by }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', trans('admin.Created_At').':') !!}
    <p>{{ $news->created_at->toDayDateTimeString() }}</p>
</div>
