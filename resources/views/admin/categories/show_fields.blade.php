<!-- Icon Field -->
<div class="col-sm-6">
    {!! Form::label('icon', trans('admin.Icon').':') !!}
    <p><img src="{{ $category->icon }}" width="150" alt="@lang('admin.Loading_image')"></p>
</div>

<!-- Title Field -->
<div class="col-sm-6">
    {!! Form::label('title', trans('admin.Title').': ('.trans('admin.English') .')') !!}
    <p>{!! $category->translate('en')->title !!}</p>
</div>
<!-- Title Field -->
<div class="col-sm-6">
    {!! Form::label('title', trans('admin.Title').': ('.trans('admin.Arabic') .')') !!}
    <p>{!! $category->translate('ar')->title !!}</p>
</div>

<!-- Description Field -->
<div class="col-sm-6">
    {!! Form::label('description', trans('admin.Description').': ('.trans('admin.English') .')') !!}
    <p>{!! $category->translate('en')->description !!}</p>
</div>
<!-- Description Field -->
<div class="col-sm-6">
    {!! Form::label('description', trans('admin.Description').': ('.trans('admin.Arabic') .')') !!}
    <p>{!! $category->translate('ar')->description !!}</p>
</div>

<!-- Is Active Field -->
<div class="col-sm-6">
    {!! Form::label('is_active', trans('admin.Is_Active').':') !!}
    <p>{{ HelpersFun::isTrue($category->is_active) }}</p>
</div>

<!-- Order By Field -->
<div class="col-sm-6">
    {!! Form::label('order_by', trans('admin.Order_By').':') !!}
    <p>{{ $category->order_by }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-6">
    {!! Form::label('created_at', trans('admin.Created_At').':') !!}
    <p>{{ $category->created_at->toDayDateTimeString() }}</p>
</div>
