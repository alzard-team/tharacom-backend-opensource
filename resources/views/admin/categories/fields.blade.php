<!-- Icon Field -->
<div class="form-group col-sm-12">
    {!! Form::label('icon', trans('admin.Icon').':') !!}
    <input type="file" class="form-control @error('icon') is-invalid @enderror" name="icon" accept="image/*">
</div>

<!-- Title Field EN -->
<div class="form-group col-sm-6">
    {!! Form::label('title', trans('admin.Title').': ('.trans('admin.English') .')') !!}
    {!! Form::text('en[title]', $category->translate('en')->title, ['class' => 'form-control']) !!}
</div>
<!-- Title Field AR -->
<div class="form-group col-sm-6">
    {!! Form::label('title', trans('admin.Title').': ('.trans('admin.Arabic') .')') !!}
    {!! Form::text('ar[title]', $category->translate('ar')->title, ['class' => 'form-control']) !!}
</div>

<!-- Description Field EN -->
<div class="form-group col-sm-6">
    {!! Form::label('description', trans('admin.Description').': ('.trans('admin.English') .')') !!}
    {!! Form::textarea('en[description]', $category->translate('en')->description, ['class' => 'form-control', 'rows' => 4]) !!}
</div>
<!-- Description Field AR -->
<div class="form-group col-sm-6">
    {!! Form::label('description', trans('admin.Description').': ('.trans('admin.Arabic') .')') !!}
    {!! Form::textarea('ar[description]', $category->translate('ar')->description, ['class' => 'form-control', 'rows' => 4]) !!}
</div>

<!-- Is Active Field -->
<div class="form-group col-sm-3">
    {!! Form::label('is_active', trans('admin.Is_Active').':') !!}
    {!! Form::select('is_active', [1 => trans('admin.Yes'), 0 => trans('admin.No')], null, ['class' => 'form-control']) !!}
</div>


<!-- Order By Field -->
<div class="form-group col-sm-3">
    {!! Form::label('order_by', trans('admin.Order_By').':') !!}
    {!! Form::number('order_by', null, ['class' => 'form-control']) !!}
</div>
