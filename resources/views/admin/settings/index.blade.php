@extends('admin.layouts.app')
@section('title', HelpersFun::settings()->site_title . ' | ' . $titlePage)
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>@lang('admin.Edit_Settings')</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($settings, ['route' => ['Admin::settings.update'], 'method' => 'post', 'enctype'=>'multipart/form-data']) !!}

            <div class="card-footer">
                {!! Form::submit(trans('admin.Save'), ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('Admin::settings.index') }}" class="btn btn-default">@lang('admin.Cancel')</a>
            </div>

            <div class="card-body">
                <div class="row">

                    {{-- General --}}
                    <div class="col-md-12">
                        <div class="card card-secondary">
                            <div class="card-header">
                                <h3 class="card-title">@lang('admin.General')</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body" style="display: block;">
                                <div class="row">
                                    <!-- site_favicon Field -->
                                    <div class="form-group col-md-6">
                                        {!! Form::label('site_favicon', trans('admin.site_favicon').':') !!}
                                        <input type="file" class="form-control @error('site_favicon') is-invalid @enderror" name="site_favicon" accept="image/*">
                                    </div>

                                    <!-- site_logo Field -->
                                    <div class="form-group col-md-6">
                                        {!! Form::label('site_logo', trans('admin.site_logo').':') !!}
                                        <input type="file" class="form-control @error('site_logo') is-invalid @enderror" name="site_logo" accept="image/*">
                                    </div>

                                    <!-- site_title Field EN -->
                                    <div class="form-group col-md-6">
                                        {!! Form::label('site_title', trans('admin.site_title').': ('.trans('admin.English') .')') !!}
                                        {!! Form::text('en[site_title]', $settings->translate('en')->site_title, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- site_site_title Field AR -->
                                    <div class="form-group col-md-6">
                                        {!! Form::label('site_title', trans('admin.site_title').': ('.trans('admin.Arabic') .')') !!}
                                        {!! Form::text('ar[site_title]', $settings->translate('ar')->site_title, ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- contacts_call Field EN -->
                                    <div class="form-group col-md-6">
                                        {!! Form::label('contacts_call', trans('admin.contacts_call').':') !!}
                                        {!! Form::text('contacts_call', $settings->contacts_call, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- contacts_email Field EN -->
                                    <div class="form-group col-md-6">
                                        {!! Form::label('contacts_email', trans('admin.contacts_email').':') !!}
                                        {!! Form::email('contacts_email', $settings->contacts_email, ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- location_on_map_url Field -->
                                    <div class="form-group col-md-12">
                                        {!! Form::label('location_on_map_url', trans('admin.location_on_map_url').':') !!}
                                        {!! Form::url('location_on_map_url', $settings->location_on_map_url, ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- location_on_map_url Field -->
                                    <div class="form-group col-md-6">
                                        {!! Form::label('google_play_url', trans('admin.google_play_url').':') !!}
                                        {!! Form::url('google_play_url', $settings->google_play_url, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- app_store_url Field -->
                                    <div class="form-group col-md-6">
                                        {!! Form::label('app_store_url', trans('admin.app_store_url').':') !!}
                                        {!! Form::url('app_store_url', $settings->app_store_url, ['class' => 'form-control']) !!}
                                    </div>

                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    {{-- General - End --}}

                    {{-- Hero Section --}}
                    <div class="col-md-12">
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">@lang('admin.Hero_Section')</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body" style="display: block;">
                                <div class="row">
                                    <!-- hero_section_title Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('hero_section_title', trans('admin.hero_section_title').': ('.trans('admin.English') .')') !!}
                                        {!! Form::text('en[hero_section_title]', $settings->translate('en')->hero_section_title, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- hero_section_title Field AR -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('hero_section_title', trans('admin.hero_section_title').': ('.trans('admin.Arabic') .')') !!}
                                        {!! Form::text('ar[hero_section_title]', $settings->translate('ar')->hero_section_title, ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- hero_section_description Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('hero_section_description', trans('admin.hero_section_description').': ('.trans('admin.English') .')') !!}
                                        {!! Form::textarea('en[hero_section_description]', $settings->translate('en')->hero_section_description, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- hero_section_description Field AR -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('hero_section_description', trans('admin.hero_section_description').':' . ' Arabic') !!}
                                        {!! Form::textarea('ar[hero_section_description]', $settings->translate('ar')->hero_section_description, ['class' => 'form-control']) !!}
                                    </div>

                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    {{-- Hero Section - End --}}

                    {{-- Features Section --}}
                    <div class="col-md-12">
                        <div class="card card-indigo">
                            <div class="card-header">
                                <h3 class="card-title">@lang('admin.Features_Section')</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body" style="display: block;">
                                <div class="row">
                                    <!-- features_video_background Field -->
                                    <div class="form-group col-md-6">
                                        {!! Form::label('features_video_background', trans('admin.features_video_background').':') !!}
                                        <input type="file" class="form-control @error('features_video_background') is-invalid @enderror" name="features_video_background" accept="image/*">
                                    </div>

                                    <!-- features_video_id Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('features_video_id', trans('admin.features_video_id').':') !!}
                                        {!! Form::text('features_video_id', $settings->features_video_id, ['class' => 'form-control']) !!}
                                    </div>

                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    {{-- Features Section - End --}}

                    {{-- Privacy Policy Section --}}
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">@lang('admin.Privacy_Policy_Section')</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body" style="display: block;">
                                <div class="row">
                                    <!-- privacy_policy_content Field EN -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('privacy_policy_content', trans('admin.privacy_policy_content').': ('.trans('admin.English') .')') !!}
                                        <textarea class="form-control summernote" name="en[privacy_policy_content]" required>{!! $settings->translate('en')->privacy_policy_content !!}</textarea>
                                    </div>
                                    <!-- privacy_policy_content Field AR -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('privacy_policy_content', trans('admin.privacy_policy_content').':' . ' Arabic') !!}
                                        <textarea class="form-control summernote" name="ar[privacy_policy_content]" required>{!! $settings->translate('ar')->privacy_policy_content !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    {{-- Privacy Policy Section - End --}}

                    {{-- About Section --}}
                    <div class="col-md-12">
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">@lang('admin.About_Section')</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body" style="display: block;">
                                <div class="row">
                                    <!-- site_title Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('about_title', trans('admin.about_title').': ('.trans('admin.English') .')') !!}
                                        {!! Form::text('en[about_title]', $settings->translate('en')->about_title, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- site_about_title Field AR -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('about_title', trans('admin.about_title').': ('.trans('admin.Arabic') .')') !!}
                                        {!! Form::text('ar[about_title]', $settings->translate('ar')->about_title, ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- about_description Field EN -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('about_description', trans('admin.about_description').': ('.trans('admin.English') .')') !!}
                                        {{--{!! Form::text('en[about_description]', $settings->translate('en')->about_description, ['class' => 'form-control summernote']) !!}--}}
                                        <textarea class="form-control summernote" name="en[about_description]" required>{!! $settings->translate('en')->about_description !!}</textarea>
                                    </div>
                                    <!-- about_description Field AR -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('about_description', trans('admin.about_description').':' . ' Arabic') !!}
                                        {{--{!! Form::text('ar[about_description]', $settings->translate('ar')->about_description, ['class' => 'form-control summernote']) !!}--}}
                                        <textarea class="form-control summernote" name="ar[about_description]" required>{!! $settings->translate('ar')->about_description !!}</textarea>
                                    </div>

                                    <!-- about_vision Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('about_vision', trans('admin.about_vision').': ('.trans('admin.English') .')') !!}
                                        {!! Form::textarea('en[about_vision]', $settings->translate('en')->about_vision, ['class' => 'form-control', 'rows'=>'4']) !!}
                                    </div>
                                    <!-- about_vision Field AR -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('about_vision', trans('admin.about_vision').':' . ' Arabic') !!}
                                        {!! Form::textarea('ar[about_vision]', $settings->translate('ar')->about_vision, ['class' => 'form-control', 'rows'=>'4']) !!}
                                    </div>

                                    <!-- about_mission Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('about_mission', trans('admin.about_mission').': ('.trans('admin.English') .')') !!}
                                        {!! Form::textarea('en[about_mission]', $settings->translate('en')->about_mission, ['class' => 'form-control', 'rows'=>'4']) !!}
                                    </div>
                                    <!-- about_mission Field AR -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('about_mission', trans('admin.about_mission').':' . ' Arabic') !!}
                                        {!! Form::textarea('ar[about_mission]', $settings->translate('ar')->about_mission, ['class' => 'form-control', 'rows'=>'4']) !!}
                                    </div>

                                    <!-- about_value Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('about_value', trans('admin.about_value').': ('.trans('admin.English') .')') !!}
                                        {!! Form::textarea('en[about_value]', $settings->translate('en')->about_value, ['class' => 'form-control', 'rows'=>'4']) !!}
                                    </div>
                                    <!-- about_value Field AR -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('about_value', trans('admin.about_value').':' . ' Arabic') !!}
                                        {!! Form::textarea('ar[about_value]', $settings->translate('ar')->about_value, ['class' => 'form-control', 'rows'=>'4']) !!}
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    {{-- About Section - End --}}

                    {{-- Social Media Section --}}
                    <div class="col-md-12">
                        <div class="card card-purple">
                            <div class="card-header">
                                <h3 class="card-title">@lang('admin.Social_Media')</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body" style="display: block;">
                                <div class="row">
                                    <!-- url_facebook Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('url_facebook', trans('admin.url_facebook').':') !!}
                                        {!! Form::url('url_facebook', $settings->url_facebook, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- url_twitter Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('url_twitter', trans('admin.url_twitter').':') !!}
                                        {!! Form::url('url_twitter', $settings->url_twitter, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- url_behance Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('url_behance', trans('admin.url_behance').':') !!}
                                        {!! Form::url('url_behance', $settings->url_behance, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- url_dribbble Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('url_dribbble', trans('admin.url_dribbble').':') !!}
                                        {!! Form::url('url_dribbble', $settings->url_dribbble, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- url_linkedin Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('url_linkedin', trans('admin.url_linkedin').':') !!}
                                        {!! Form::url('url_linkedin', $settings->url_linkedin, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- url_instagram Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('url_instagram', trans('admin.url_instagram').':') !!}
                                        {!! Form::url('url_instagram', $settings->url_instagram, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- url_youtube Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('url_youtube', trans('admin.url_youtube').':') !!}
                                        {!! Form::url('url_youtube', $settings->url_youtube, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- url_whatsapp Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('url_whatsapp', trans('admin.url_whatsapp').':') !!}
                                        {!! Form::url('url_whatsapp', $settings->url_whatsapp, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- url_vimeo Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('url_vimeo', trans('admin.url_vimeo').':') !!}
                                        {!! Form::url('url_vimeo', $settings->url_vimeo, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- url_rss Field EN -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('url_rss', trans('admin.url_rss').':') !!}
                                        {!! Form::url('url_rss', $settings->url_rss, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    {{-- Social Media Section - End--}}

                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit(trans('admin.Save'), ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('Admin::settings.index') }}" class="btn btn-default">@lang('admin.Cancel')</a>
            </div>

           {!! Form::close() !!}

        </div>
    </div>
@endsection


@push('page_css')
    <!-- summernote -->
    <link href="//cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    {{--<link rel="stylesheet" href="{!! asset('/adminlte/plugins/summernote/css/summernote-bs4.min.css') !!}">--}}
@endpush

@push('page_scripts')
    <!-- Summernote -->
    <script src="//cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
    {{--<script src="{!! asset('/adminlte/plugins/summernote/js/summernote-bs4.min.js') !!}"></script>--}}
    <script>
        $(function () {
            // Summernote
            $('.summernote').summernote();

            // CodeMirror
            /*CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });*/
        })
    </script>
@endpush
